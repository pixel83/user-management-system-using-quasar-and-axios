const routes = [
	{
		path: "/",
		component: () => import("layouts/MainLayout.vue"),
		children: [
			{ path: "", component: () => import("src/pages/UserForm.vue") },
		],
	},
	{
		path: "/",
		component: () => import("layouts/MainLayout.vue"),
		children: [
			{
				path: "user-form",
				name: "user-form",
				component: () => import("pages/UserForm.vue"),
			},
			{
				path: "user-list",
				name: "user-list",
				component: () => import("pages/UserList.vue"),
			},
		],
	},

	// Always leave this as last one,
	// but you can also remove it
	{
		path: "/:catchAll(.*)*",
		component: () => import("pages/ErrorNotFound.vue"),
	},
];

export default routes;
